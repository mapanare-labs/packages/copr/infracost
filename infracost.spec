%define debug_package %{nil}

Name:           infracost
Version:        0.10.39
Release:        1%{?dist}
Summary:        Cloud cost estimates for Terraform in pull requests. Love your cloud bill!

License:        ASL 2.0
URL:            https://infracost.io/
Source0:        https://github.com/%{name}/%{name}/releases/download/v%{version}/%{name}-linux-amd64.tar.gz

%description
Cloud cost estimates for Terraform in pull requests
Love your cloud bill!

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_bindir}
install -m 755 %{name}-linux-amd64 %{buildroot}/%{_bindir}/%{name}

%files
%{_bindir}/%{name}

%changelog
* Tue Nov 12 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version v0.10.39
* Fri Jul 19 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version v0.10.38 
* Mon Jun 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue May 28 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Wed Apr 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Thu Feb 15 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.10.33

* Thu Dec 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.10.31

* Wed Nov 15 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.10.30

* Thu Aug 31 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.10.28

* Wed Jul 26 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.10.26

* Thu May 18 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.10.22

* Mon May 08 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.10.21

* Tue Apr 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.10.20

* Fri Feb 10 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.10.17

* Wed Dec 14 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.10.15

* Tue Nov 01 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.10.13

* Thu Sep 15 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM
